# checksummator

A simple tool to create and compare checksums for many files.

* Requires Python 3.
* Compares file name, size, modification time, and CRC32 checksum of file content.
* Uses multiple parallel processes.

## Use

```
checksummator.py create -d /my/big/directory
```
scans all files in `/my/big/directory` and writes checksums to the file `checksums.sqlite`.

The next run with `create` will rename an existing checksum file to `checksums.sqlite.old` and create a new 
file `checksums.sqlite`.

```
checksummator.py compare
```
reads `checksums.sqlite.old` and `checksums.sqlite` and shows the number of added, removed and changed files.

```
checksummator.py update -d /my/big/directory
```
runs both a `create` and a `compare` action, showing differences to the last time `create` or `update` was run.

For more options see `checksummator -h`:
```
usage: checksummator.py [-h] [--version] [-d BASEDIR] [-f CHECKSUMFILE] [-c COMPAREFILE]
                        [--json-file JSONFILE] [--csv-file CSVFILE] [-l {INFO,DEBUG,ERROR}]
                        [--include-dotfiles] [--exclude EXCLUDELIST] [--exclude-file EXCLUDEFILE]
                        [--show-added] [--show-removed] [--show-modified] [--show-modified-details]
                        [--num-procs NUM_PROCS] [--debug-progress DEBUG_PROGRESS] [--no-checksums]
                        {create,compare,update,load-json,save-json,save-csv}

Create and compare file checksums.

positional arguments:
  {create,compare,update,load-json,save-json,save-csv}
                        Action: create=create new checksum file, compare=show differences to other
                        checksum file, update=create new checksums and show differences to old
                        checksums, load-json=load JSON file and save as sqlite file, save-json=load
                        sqlite file and save as JSON file.save-csv=load sqlite file and save as CSV
                        file.

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -d BASEDIR, --basedir BASEDIR
                        Base directory to be scanned with all subdirectories.
  -f CHECKSUMFILE, --file CHECKSUMFILE
                        Checksum file to create.
  -c COMPAREFILE, --compare COMPAREFILE
                        Checksum file to compare.
  --json-file JSONFILE  JSON file to load or save.
  --csv-file CSVFILE    CSV file to save.
  -l {INFO,DEBUG,ERROR}, --log {INFO,DEBUG,ERROR}
                        Log level.
  --include-dotfiles    Include files and directories starting with a dot.
  --exclude EXCLUDELIST
                        Comma-separated list of file or directory names to exclude.
  --exclude-file EXCLUDEFILE
                        File containing file or directory names to exclude.
  --show-added          List added files.
  --show-removed        List removed files.
  --show-modified       List modified files.
  --show-modified-details
                        List details of modified files.
  --num-procs NUM_PROCS
                        Number of parallel processes.
  --debug-progress DEBUG_PROGRESS
                        Period of progress messages (every X files, at DEBUG level).
  --no-checksums        Do not create checksums, use only file size and date.
```
