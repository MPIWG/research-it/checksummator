#!/usr/bin/env python3

import os
import hashlib
import zlib
import concurrent.futures
import multiprocessing
import json
import stat
import logging
import time
import argparse
import sys


NUM_PROCS = 5 # number of parallel processes
BLOCKSIZE = 65536 # block size when reading file
CHUNKSIZE = 100 # number of files handed to process worker
EXCLUDE_DOT = True # exclude dot files and directories
EXCLUDE_NAMES = None # set of excluded file names
DEBUG_PROGRESS = 100000 # period of progress messages
NO_CHECKSUMS = False # do not create checksums

_HASH_COUNTER = multiprocessing.Value('i', 0)

def create_file_list(basedir):
    """
    Create list of all files in basedir and it subdirectories.
    
    Returns file list.
    """
    # list of files to be processed
    file_list = []
    fcnt = 0
    
    for directory,subdirs,files in os.walk(basedir):
        if EXCLUDE_DOT:
            files = [f for f in files if not f[0] == '.']
            subdirs[:] = [d for d in subdirs if not d[0] == '.']
            
        if EXCLUDE_NAMES:
            files = [f for f in files if not f in EXCLUDE_NAMES]
            subdirs[:] = [d for d in subdirs if not d in EXCLUDE_NAMES]
            
        for fn in files:
            path = os.path.join(directory, fn)
            file_list.append(path)
    
            fcnt += 1
            if fcnt % DEBUG_PROGRESS == 0:
                logging.debug("listed {} files".format(fcnt))
                
            #if fcnt == 10000:
            #    return file_list
    
    return file_list

def _inc_hash_cnt():
    with _HASH_COUNTER.get_lock():
        _HASH_COUNTER.value += 1
        if _HASH_COUNTER.value % DEBUG_PROGRESS == 0:
            logging.debug("hashed {} files".format(_HASH_COUNTER.value))        

def hash_file_crc(f):
    """
    Returns file_info for file f using CRC32.
    
    file_info dict contains file path, size, mtime and crc32.
    """    
    file_info = {}
    stat_info = os.lstat(f)
    if stat.S_ISREG(stat_info.st_mode):
        #  f is a file
        file_info['path'] = f
        file_info['size'] = stat_info.st_size
        file_info['mtime'] = int(stat_info.st_mtime)
    else:
        return None

    if NO_CHECKSUMS:
        return file_info

    try:
        checksum = 0
        with open(f, 'rb') as afile:
            buf = afile.read(BLOCKSIZE)
            while len(buf) > 0:
                checksum = zlib.crc32(buf, checksum)
                #checksum = zlib.adler32(buf, checksum)
                buf = afile.read(BLOCKSIZE)
                
        file_info['crc32'] = hex(checksum)
        
    except OSError as e:
        logging.error("ERROR reading file {}: {}".format(f, e))
        
    _inc_hash_cnt()
    return file_info

def hash_file_md5(f):
    """
    Returns file_info for file f using MD5.
    
    file_info dict contains file path, size, mtime and md5.
    """    
    file_info = {}
    stat_info = os.lstat(f)
    if stat.S_ISREG(stat_info.st_mode):
        #  f is a file
        file_info['path'] = f
        file_info['size'] = stat_info.st_size
        file_info['mtime'] = int(stat_info.st_mtime)
    else:
        return None

    if NO_CHECKSUMS:
        return file_info

    try:
        hasher = hashlib.md5()
        with open(f, 'rb') as afile:
            #print(f)
            buf = afile.read(BLOCKSIZE)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(BLOCKSIZE)
                
        file_info['md5'] = hasher.hexdigest()
        
    except OSError as e:
        logging.error("ERROR reading file {}: {}".format(f, e))
        
    _inc_hash_cnt()
    return file_info

def create_hashes(basedir):
    """
    Hash all files in basedir.
    
    Returns a dict with filenames as keys and file info dicts as values.
    """
    # crawl directories
    logging.info("creating file list...")
    next_time = time.time()
    
    file_list = create_file_list(basedir)
    logging.info("found {} files".format(len(file_list)))
    logging.debug("  took {}s".format(time.time() - next_time))
    
    logging.info("creating hashes...")
    _HASH_COUNTER.value = 0
    next_time = time.time()
    
    # hash all files using concurrent executor
    with concurrent.futures.ProcessPoolExecutor(max_workers=NUM_PROCS) as executor:
        logging.debug("  hashing {} files using {} workers".format(len(file_list), NUM_PROCS))
        hash_list = executor.map(hash_file_crc, file_list, chunksize=CHUNKSIZE)
    
    logging.debug("  took {}s".format(time.time() - next_time))
    logging.debug("  counted {} hashes".format(_HASH_COUNTER.value))
    
    # pack result hash using file path
    file_hashes = {}
    for info in hash_list:
        if info is None:
            continue
        
        path = info['path']
        del info['path']
        file_hashes[path] = info
    
    logging.info("created {} hashes".format(len(file_hashes)))
    return file_hashes

def save_hashes(file_hashes, outfilename):
    """
    Save file_hashes as outfilename
    """
    # write file_hashes "filename -> md5sum" to disk
    logging.debug("writing json file {}".format(outfilename))
    next_time = time.time()
    
    with open(outfilename, "w") as file:
        json.dump(file_hashes, file)
    
    logging.debug("  took {}s".format(time.time() - next_time))

def load_hashes(infilename):
    """
    Load infilename and return file_hashes
    """
    logging.debug("loading json file {}".format(infilename))
    next_time = time.time()
    
    with open(infilename, "r") as file:
        file_hashes = json.load(file)
    
    logging.debug("  took {}s".format(time.time() - next_time))
    return file_hashes

def compare_hashes(file_info1, file_info2):
    """
    Compare file_info1 (before) with file_info2 (after).
    
    Returns (added_fns, removed_fns, modified_info)
    where added_fns and removed_fns sets of file names
    and modified_info is a dict {fn: (file_info1, file_info2)}
    """
    logging.debug("comparing hashes")
    next_time = time.time()
    
    fns1 = set(file_info1.keys())
    fns2 = set(file_info2.keys())
    common_fns = fns1.intersection(fns2)
    # added fns are in fns2 but not in fns1
    added_fns = fns2 - fns1
    removed_fns = fns1 - fns2
    # check common_fns for different hashes
    different_info = {fn : (file_info1[fn], file_info2[fn]) for fn in common_fns if file_info1[fn] != file_info2[fn]}
    # narrow down to keys with different values
    modified_info = {}
    for fn in different_info:
        full_info1, full_info2 = different_info[fn]
        diff_info1 = {}
        diff_info2 = {}
        # process different items
        for key, _ in (full_info1.items() - full_info2.items()):
            if NO_CHECKSUMS and key == 'crc32':
                continue
            
            diff_info1[key] = full_info1[key]
            diff_info2[key] = full_info2.get(key, None)
            
        if diff_info1:
            modified_info[fn] = (diff_info1, diff_info2)
            
    logging.debug("  took {}s".format(time.time() - next_time))
    return added_fns, removed_fns, modified_info
    
def action_create(args):
    if args.basedir is None:
        sys.exit('ERROR: missing basedir parameter!')
    logging.info("Reading files in {} and creating hash file {}".format(args.basedir, args.checksumfile))
    if os.path.exists(args.checksumfile):
        os.rename(args.checksumfile, args.checksumfile + '.old')
    file_hashes = create_hashes(args.basedir)
    save_hashes(file_hashes, args.checksumfile)

def action_compare(args):
    logging.info("Comparing hash file {} with {}".format(args.comparefile, args.checksumfile))
    file_hashes1 = load_hashes(args.comparefile)
    file_hashes2 = load_hashes(args.checksumfile)
    added_fns, removed_fns, modified_hashes = compare_hashes(file_hashes1, file_hashes2)
    logging.info("{} files added".format(len(added_fns)))
    logging.info("{} files removed".format(len(removed_fns)))
    logging.info("{} files modified".format(len(modified_hashes)))
    if added_fns and args.show_added:
        logging.info("Added files:")
        for fn in added_fns:
            print(fn)
    
    if removed_fns and args.show_removed:
        logging.info("Removed files:")
        for fn in removed_fns:
            print(fn)
    
    if modified_hashes and args.show_modified or args.show_modified_details:
        logging.info("Modified files:")
        for fn in modified_hashes.keys():
            if args.show_modified_details:
                before, after = modified_hashes[fn]
                print("{} : before={}, after={}".format(fn, before, after))
            else:
                print(fn)


##
## main
##
def main():
    argp = argparse.ArgumentParser(description='Create and compare file checksums.')
    argp.add_argument('--version', action='version', version='%(prog)s 1.3')
    argp.add_argument('action', choices=['create', 'compare', 'update'], default='create', 
                      help='Action: create=create new checksum file, '
                      + 'compare=show differences to other checksum file, '
                      + 'update=create new checksums and show differences to old checksums.')
    argp.add_argument('-d', '--basedir', dest='basedir',
                      help='Base directory to be scanned with all subdirectories.')
    argp.add_argument('-f', '--file', dest='checksumfile', default='checksums.json', 
                      help='Checksum file to create.')
    argp.add_argument('-c', '--compare', dest='comparefile', default='checksums.json.old', 
                      help='Checksum file to compare.')
    argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                      help='Log level.')
    argp.add_argument('--include-dotfiles', dest='dotfiles', action='store_true', 
                      help='Include files and directories starting with a dot.')
    argp.add_argument('--exclude', dest='excludelist',
                      help='Comma-separated list of file or directory names to exclude.')
    argp.add_argument('--exclude-file', dest='excludefile',
                      help='File containing file or directory names to exclude.')
    argp.add_argument('--show-added', dest='show_added', action='store_true', 
                      help='List added files.')
    argp.add_argument('--show-removed', dest='show_removed', action='store_true', 
                      help='List removed files.')
    argp.add_argument('--show-modified', dest='show_modified', action='store_true', 
                      help='List modified files.')
    argp.add_argument('--show-modified-details', dest='show_modified_details', action='store_true', 
                      help='List details of modified files.')
    argp.add_argument('--num-procs', dest='num_procs', type=int, default=5,
                      help='Number of parallel processes.')
    argp.add_argument('--debug-progress', dest='debug_progress', type=int, default=100000,
                      help='Period of progress messages (every X files, at DEBUG level).')
    argp.add_argument('--no-checksums', dest='no_checksums', action='store_true',
                      help='Do not create checksums, use only file size and date.')
    args = argp.parse_args()
    
    # set up 
    logging.basicConfig(level=args.loglevel)
    global EXCLUDE_DOT, EXCLUDE_NAMES
    EXCLUDE_DOT = not args.dotfiles
    if args.excludelist:
        EXCLUDE_NAMES = set(args.excludelist.split(','))
    elif args.excludefile:
        with open(args.excludefile, "rt", encoding='utf-8') as file:
            EXCLUDE_NAMES = set();
            for line in file:
                EXCLUDE_NAMES.add(line.strip())

    global NUM_PROCS, DEBUG_PROGRESS, NO_CHECKSUMS
    NUM_PROCS = args.num_procs
    DEBUG_PROGRESS = args.debug_progress
    NO_CHECKSUMS = args.no_checksums
    
    # actions
    if args.action == 'create':
        action_create(args)
    
    elif args.action == 'compare':
        action_compare(args)
    
    elif args.action == 'update':
        action_create(args)
        action_compare(args)
    
    logging.info("done.")

if __name__ == '__main__':
    main()
