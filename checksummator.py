#!/usr/bin/env python3

import argparse
import concurrent.futures
import json
import logging
import os
import sqlite3
import stat
import sys
import time
import zlib
import datetime

__version__ = '2.2'

NUM_PROCS = 5 # number of parallel processes
BLOCKSIZE = 65536 # block size when reading file
PROCESS_CHUNKSIZE = 100 # number of files handed to process worker
CHUNKSIZE = 10000 # number of files processed at a time
EXCLUDE_DOT = True # exclude dot files and directories
EXCLUDE_NAMES = None # set of excluded file names
DEBUG_PROGRESS = 100000 # period of progress messages
NO_CHECKSUMS = False # do not create checksums
CSV_SEP = ',' # CSV file record separator

def create_file_list(basedir, db):
    """
    Create list of all files in basedir and it subdirectories.
    
    Creates file list in database (connection) db.
    Returns file count.
    """
    # list of files to be processed
    fcnt = 0
    
    for directory, subdirs, files in os.walk(basedir):
        if EXCLUDE_DOT:
            files = [f for f in files if not f[0] == '.']
            subdirs[:] = [d for d in subdirs if not d[0] == '.']
            
        if EXCLUDE_NAMES:
            files = [f for f in files if not f in EXCLUDE_NAMES]
            subdirs[:] = [d for d in subdirs if not d in EXCLUDE_NAMES]
            
        paths = []
        for fn in files:
            path = os.path.join(directory, fn)
            paths.append((path,))
            
            fcnt += 1
            if fcnt % DEBUG_PROGRESS == 0:
                logging.debug("listed {} files".format(fcnt))
                
        db.executemany('INSERT INTO files (path) VALUES (?)', paths)
        db.commit()
    return fcnt

def hash_file_crc(fid):
    """
    Returns file_info for file f using CRC32.
    
    file_info dict contains file path, size, mtime and crc32.
    """    
    file_info = {}
    (f, db_id) = fid
    try:
        stat_info = os.lstat(f)
        if stat.S_ISREG(stat_info.st_mode):
            #  f is a file
            file_info['id'] = db_id
            file_info['path'] = f
            file_info['size'] = stat_info.st_size
            file_info['mtime'] = int(stat_info.st_mtime)
        else:
            return None
    
        if NO_CHECKSUMS:
            return file_info

        if stat_info.st_size > 0:
            checksum = 0
            with open(f, 'rb') as afile:
                buf = afile.read(BLOCKSIZE)
                while len(buf) > 0:
                    checksum = zlib.crc32(buf, checksum)
                    buf = afile.read(BLOCKSIZE)
                    
            file_info['hash'] = hex(checksum)
        
    except OSError as e:
        logging.error("ERROR reading file {}: {}".format(f, e))
        
    return file_info

def create_hashes(basedir, db):
    """
    Hash all files in basedir.
    
    Adds file info to files table in db.
    """
    # crawl directories
    logging.info("creating file list...")
    next_time = time.time()
    
    filecnt = create_file_list(basedir, db)
    logging.info("found {} files".format(filecnt))
    logging.debug("  took {}s".format(time.time() - next_time))
    
    logging.info("creating hashes...")
    next_time = time.time()

    hashcnt = 0
    num_files = db.execute('SELECT count(*) FROM files').fetchone()[0]
    # hash all files using concurrent executor
    #with concurrent.futures.ThreadPoolExecutor(max_workers=NUM_PROCS) as executor:
    with concurrent.futures.ProcessPoolExecutor(max_workers=NUM_PROCS) as executor:
        logging.debug("  hashing {} files using {} workers".format(num_files, NUM_PROCS))
        for offset in range(0, num_files, CHUNKSIZE):
            paths_res = db.execute("SELECT path, rowid FROM files LIMIT %s OFFSET %s"%(CHUNKSIZE, offset))
            # hash the current list of paths in parallel
            hash_list = executor.map(hash_file_crc, paths_res, chunksize=PROCESS_CHUNKSIZE)
            # update database using result list
            param_list = [(i['size'], i['mtime'], i.get('hash', ''), i['id']) for i in hash_list if i]
            db.executemany('UPDATE files SET size = ?, mtime = ?, hash = ? WHERE rowid = ?', param_list)
            db.commit()
 
            hashcnt += len(param_list)
            if offset % DEBUG_PROGRESS == 0:
                logging.debug("hashed {} files".format(hashcnt))

    logging.debug("  took {}s".format(time.time() - next_time))
    logging.info("created {} hashes".format(hashcnt))
    return hashcnt

def compare_sql(db):
    """
    Compare file table in 'main' db (new) with file table in 'other' db (old).
    
    Returns (added_fns, removed_fns, modified_info)
    where added_fns and removed_fns sets of file names
    and modified_info is a dict {fn: (file_info_before, file_info_after)}
    """
    logging.debug("comparing files in db")
    next_time = time.time()

    nfiles_main = db.execute('SELECT count(*) from main.files').fetchone()[0]
    nfiles_other = db.execute('SELECT count(*) from other.files').fetchone()[0]
    logging.debug("  {} files in old db, {} files in new db".format(nfiles_other, nfiles_main))

    # added files
    res = db.execute('''
    SELECT path 
    FROM main.files mfiles LEFT OUTER JOIN other.files ofiles USING(path)
    WHERE ofiles.path IS NULL
    ''')
    added_fns = [fn[0] for fn in res]

    # removed files
    res = db.execute('''
    SELECT path 
    FROM other.files ofiles LEFT OUTER JOIN main.files mfiles USING(path)
    WHERE mfiles.path IS NULL
    ''')
    removed_fns = [fn[0] for fn in res]

    # files with different attributes
    res = db.execute('''
    SELECT mfiles.path, mfiles.size, mfiles.mtime, mfiles.hash, 
        ofiles.path, ofiles.size, ofiles.mtime, ofiles.hash
    FROM main.files mfiles JOIN other.files ofiles USING(path)
    WHERE mfiles.size != ofiles.size OR mfiles.mtime != ofiles.mtime OR mfiles.hash != ofiles.hash
    ''')
    # narrow down to keys with different values
    modified_info = {}
    for infos in res:
        m_path, m_size, m_mtime, m_hash, o_path, o_size, o_mtime, o_hash = infos
        m_info = {}
        o_info = {}
        if m_path != o_path:
            # this should not happen
            logging.error("file path changed in JOIN! {} vs {}".format(m_path, o_path))
            sys.exit(2)

        if m_size != o_size:
            m_info['size'] = m_size
            o_info['size'] = o_size

        if m_mtime != o_mtime:
            m_info['mtime'] = m_mtime
            o_info['mtime'] = o_mtime

        if m_hash != o_hash:
            m_info['hash'] = m_hash
            o_info['hash'] = o_hash

        if not m_info or not o_info:
            # this should not happen
            logging.error("no difference in modified attributes! {} vs {}".format(m_path, o_path))
            sys.exit(2)
        
        modified_info[m_path] = (o_info, m_info)

    logging.debug("  took {}s".format(time.time() - next_time))
    return added_fns, removed_fns, modified_info


def create_db(args):
    """
    Create new database file and empty table.
    """
    if os.path.exists(args.checksumfile):
        os.rename(args.checksumfile, args.checksumfile + '.old')
        
    dbcon = sqlite3.connect(args.checksumfile)
    dbcon.execute('CREATE TABLE files (path TEXT, size INTEGER, mtime INTEGER, hash TEXT)')
    return dbcon

def action_create(args):
    """
    Create and fill new database.
    """
    if args.basedir is None:
        sys.exit('ERROR: missing BASEDIR parameter!')
        
    logging.info("Reading files in {} and creating hash file {}".format(args.basedir, args.checksumfile))
    dbcon = create_db(args)
    create_hashes(args.basedir, dbcon)
    dbcon.close()

def action_compare(args):
    """
    Compare contents of checksumfile database with comparefile database.
    """
    logging.info("Comparing hash database {} (old) with {} (new)".format(args.comparefile, args.checksumfile))
    dbcon = sqlite3.connect(args.checksumfile)
    dbcon.execute("ATTACH DATABASE '%s' AS other"%args.comparefile)
    added_fns, removed_fns, modified_hashes = compare_sql(dbcon)
    dbcon.close()
    logging.info("{} files added".format(len(added_fns)))
    logging.info("{} files removed".format(len(removed_fns)))
    logging.info("{} files modified".format(len(modified_hashes)))
    if added_fns and args.show_added:
        logging.info("Added files:")
        for fn in added_fns:
            print(fn)
    
    if removed_fns and args.show_removed:
        logging.info("Removed files:")
        for fn in removed_fns:
            print(fn)
    
    if modified_hashes and args.show_modified or args.show_modified_details:
        logging.info("Modified files:")
        for fn in modified_hashes.keys():
            if args.show_modified_details:
                before, after = modified_hashes[fn]
                print("{} : before={}, after={}".format(fn, before, after))
            else:
                print(fn)


def action_load_json(args):
    """
    Load jsonfile and save checksumfile
    """
    infilename = args.jsonfile
    logging.info("Loading json file {}".format(infilename))
    logging.info("Writing sqlite file {}".format(args.checksumfile))
    next_time = time.time()
    
    with open(infilename, "r") as file:
        file_infos = json.load(file)

    db = create_db(args)
    cnt = 0
    for fn, info in file_infos.items():
        db.execute('INSERT INTO files (path, size, mtime, hash) VALUES (?, ?, ?, ?)',
                   (fn, info['size'], info['mtime'], info['crc32']))
        
        cnt += 1
        if cnt % DEBUG_PROGRESS == 0:
            db.commit()
            logging.debug("  loaded {} files".format(cnt))

    db.commit()
    db.close()
    logging.debug("  written {} files".format(cnt))
    logging.debug("  took {}s".format(time.time() - next_time))

def action_save_json(args):
    """
    Load checksumfile and save jsonfile
    """
    infilename = args.checksumfile
    outfilename = args.jsonfile
    logging.info("Loading sqlite file {}".format(infilename))
    logging.info("Writing json file {}".format(outfilename))
    next_time = time.time()

    db = sqlite3.connect(infilename)
    res = db.execute('SELECT path, size, mtime, hash FROM files')
    file_hashes = {}
    for r in res:
        path, size, mtime, file_hash = r
        file_hashes[path] = {'size': size, 'mtime': mtime, 'crc32': file_hash}
    
    with open(outfilename, "w") as file:
        json.dump(file_hashes, file)
    
    logging.debug("  written {} records".format(len(file_hashes)))
    logging.debug("  took {}s".format(time.time() - next_time))

def action_save_csv(args):
    """
    Load checksumfile and save csv file
    """
    infilename = args.checksumfile
    outfilename = args.csvfile
    logging.info("Loading sqlite file {}".format(infilename))
    logging.info("Writing CSV file {}".format(outfilename))
    next_time = time.time()

    cnt = 0
    db = sqlite3.connect(infilename)
    res = db.execute('SELECT path, size, mtime, hash FROM files ORDER BY path')
    with open(outfilename, "w") as file:
        # write header
        print('filepath', 'size', 'date', 'mtime', 'checksum', file=file, sep=CSV_SEP)
        for r in res:
            path, size, mtime, file_hash = r
            if mtime:
                iso_mtime = datetime.datetime.fromtimestamp(int(mtime)).isoformat()
            else:
                iso_mtime = ''
                
            # write row
            print('"'+path+'"', size, iso_mtime, mtime, '"'+file_hash+'"', file=file, sep=CSV_SEP)
            cnt += 1
    
    logging.debug("  written {} records".format(cnt))
    logging.debug("  took {}s".format(time.time() - next_time))

##
## main
##
def main():
    argp = argparse.ArgumentParser(description='Create and compare file checksums.')
    argp.add_argument('--version', action='version', version='%(prog)s ' + __version__)
    argp.add_argument('action', choices=['create', 'compare', 'update', 'load-json', 'save-json', 'save-csv'], 
                      default='create', 
                      help='Action: create=create new checksum file, '
                      + 'compare=show differences to other checksum file, '
                      + 'update=create new checksums and show differences to old checksums, '
                      + 'load-json=load JSON file and save as sqlite file, '
                      + 'save-json=load sqlite file and save as JSON file.'
                      + 'save-csv=load sqlite file and save as CSV file.')
    argp.add_argument('-d', '--basedir', dest='basedir',
                      help='Base directory to be scanned with all subdirectories.')
    argp.add_argument('-f', '--file', dest='checksumfile', default='checksums.sqlite', 
                      help='Checksum file to create.')
    argp.add_argument('-c', '--compare', dest='comparefile', default='checksums.sqlite.old', 
                      help='Checksum file to compare.')
    argp.add_argument('--json-file', dest='jsonfile', default='checksums.json', 
                      help='JSON file to load or save.')
    argp.add_argument('--csv-file', dest='csvfile', default='checksums.csv', 
                      help='CSV file to save.')
    argp.add_argument('-l', '--log', dest='loglevel', choices=['INFO', 'DEBUG', 'ERROR'], default='INFO', 
                      help='Log level.')
    argp.add_argument('--include-dotfiles', dest='dotfiles', action='store_true', 
                      help='Include files and directories starting with a dot.')
    argp.add_argument('--exclude', dest='excludelist',
                      help='Comma-separated list of file or directory names to exclude.')
    argp.add_argument('--exclude-file', dest='excludefile',
                      help='File containing file or directory names to exclude.')
    argp.add_argument('--show-added', dest='show_added', action='store_true', 
                      help='List added files.')
    argp.add_argument('--show-removed', dest='show_removed', action='store_true', 
                      help='List removed files.')
    argp.add_argument('--show-modified', dest='show_modified', action='store_true', 
                      help='List modified files.')
    argp.add_argument('--show-modified-details', dest='show_modified_details', action='store_true', 
                      help='List details of modified files.')
    argp.add_argument('--num-procs', dest='num_procs', type=int, default=5,
                      help='Number of parallel processes.')
    argp.add_argument('--debug-progress', dest='debug_progress', type=int, default=100000,
                      help='Period of progress messages (every X files, at DEBUG level).')
    argp.add_argument('--no-checksums', dest='no_checksums', action='store_true',
                      help='Do not create checksums, use only file size and date.')
    args = argp.parse_args()
    
    # set up 
    logging.basicConfig(level=args.loglevel)
    global EXCLUDE_DOT, EXCLUDE_NAMES
    EXCLUDE_DOT = not args.dotfiles
    if args.excludelist:
        EXCLUDE_NAMES = set(args.excludelist.split(','))
    elif args.excludefile:
        with open(args.excludefile, "rt", encoding='utf-8') as file:
            EXCLUDE_NAMES = set();
            for line in file:
                EXCLUDE_NAMES.add(line.strip())

    global NUM_PROCS, DEBUG_PROGRESS, NO_CHECKSUMS
    NUM_PROCS = args.num_procs
    DEBUG_PROGRESS = args.debug_progress
    NO_CHECKSUMS = args.no_checksums
    
    # actions
    if args.action == 'create':
        action_create(args)
    
    elif args.action == 'compare':
        action_compare(args)
    
    elif args.action == 'update':
        action_create(args)
        action_compare(args)
    
    elif args.action == 'load-json':
        if not args.jsonfile:
            sys.exit('ERROR: missing JSONFILE parameter!')
        
        action_load_json(args)
    
    elif args.action == 'save-json':
        if not args.jsonfile:
            sys.exit('ERROR: missing JSONFILE parameter!')
        
        action_save_json(args)
    
    elif args.action == 'save-csv':
        if not args.csvfile:
            sys.exit('ERROR: missing CSVFILE parameter!')
        
        action_save_csv(args)
    
    logging.info("done.")

if __name__ == '__main__':
    main()
